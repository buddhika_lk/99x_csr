<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Colombo Friend-In-Need Society</title>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    
</head>

<body>
<!-- <img class="brand" src="img/Logo.jpg" width="50px" />-->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
    <div class="navbar-header">
    <a class="span1" href="index.php" ></a></div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <div class="mainL"><div class="mainlogo"><img class="img-responsive" src="img/menu_logo.jpg" alt=""></div></div>
        <ul class="nav navbar-nav pull-right">
              <li><a href="home.php">Home</a></li>
              <li><a href="about.php">About Us</a></li>
              <li><a href="services.php">Services</a></li>
                <li><a href="timeline.php" >Timeline</a></li>
                    <li><a href="gallery.php">Gallery</a></li>
                    <!-- <li><a href="doc/Gift%20Form.doc" download="Form">Donate</a></li> -->
                    <li><a href="https://docs.google.com/forms/d/1V5yzu5tB3cScYAu-2SzC_bpWledfefwmt1JWuUy7rFI/viewform" target="">Donate</a></li>
                    
              <li><a href="contact.php">Contact Us</a></li>
      </ul>           
    </div>

  </div>
</div><!--end cssmenu--> 

<!-- modal -->
<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style type="text/css">
  .navbar-default {
  background-color: #a50c10;
  border-color: #da4551;
  height: 50px;
}
.navbar-default .navbar-brand {
  color: #ffffff;
}
.navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
  color: #d8d3dc;
}
.navbar-default .navbar-text {
  color: #ffffff;
}
.navbar-default .navbar-nav > li > a {
  color: #ffffff;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
  color: #d8d3dc;
}
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
  color: #d8d3dc;
  background-color: #da4551;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  color: #d8d3dc;
  background-color: #da4551;
}
.navbar-default .navbar-toggle {
  border-color: #da4551;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
  background-color: #da4551;
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #ffffff;
}
.navbar-default .navbar-collapse,
.navbar-default .navbar-form {
  border-color: #ffffff;
}
.navbar-default .navbar-link {
  color: #ffffff;
}
.navbar-default .navbar-link:hover {
  color: #d8d3dc;
}

@media (max-width: 767px) {
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #ffffff;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #d8d3dc;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #d8d3dc;
    background-color: #da4551;
  }
}
</style>