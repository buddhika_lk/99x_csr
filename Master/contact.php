<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Colombo Friend-In-Need Society</title>
	<link rel="stylesheet" href="">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
   	<link href="slider/js-image-slider.css" rel="stylesheet" type="text/css" />
   	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.easy-ticker.js"></script>
    			
</head>
<body>


<!-- *****************************section-one**************************************************-->
<div><?php include 'header.php'; ?></div>
<div>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Contact us <small>Feel free to contact us</small></h1>
            </div>
        </div>
    </div>
    
<!-- map -->
<div id="google-map" class="embed-responsive" alt="Responsive image">
    <iframe src="https://www.google.com/maps/embed/v1/place?q=Sir+James+Pieris+Mawatha,+Colombo,+Western+Province,+Sri+Lanka&key=AIzaSyChYthwsXPLNOdNZ8_KTy2FWR3i9kKKa1Y" width="1400" height="400" frameborder="0" style="border:0"></iframe>
</div>


</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email Address</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subject</label>
                            <select id="subject" name="subject" class="form-control" required="required">
                                <option value="na" selected="">Choose One:</option>
                                <option value="service">General Customer Service</option>
                                <option value="suggestions">Suggestions</option>
                                <option value="product">Product Support</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Send Message</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
            <address>
                <strong>The Colombo Friend-In-Need Society.</strong><br>
                No. 171, Sir James Pieris Mawatha,<br>
                Colombo 2,<br>
                Sri Lanka<br>
                <abbr title="Phone">
                    Hotine:</abbr>
                +94 11 2421651<br>
                <abbr title="Phone">
                    Fax:</abbr>
                +94 11 2544992
            </address>
            <address>
                <strong>Email</strong><br>
                <a href="mailto:#">cfins@sltnet.lk</a>
            </address>
            </form>
        </div>
    </div>
</div>
</div>

<!-- end of contact us-->

	<!-- End -->
<style type="text/css">
    .jumbotron {
background: #FFF;
color: #FFF;
border-radius: 0px;
}
.jumbotron-sm { padding-top: 24px;
padding-bottom: 24px; }
.jumbotron small {
color: #FFF;
}
.h1 small {
font-size: 24px;
}
</style>
<?php //footer
include 'footer.php';
?>  
</body>
</html>