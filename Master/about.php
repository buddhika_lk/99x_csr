<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Colombo Friend-In-Need Society</title>
	<link rel="stylesheet" href="">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
   	<link href="slider/js-image-slider.css" rel="stylesheet" type="text/css" />
   	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.easy-ticker.js"></script>
    			
</head>
<body>


<!-- *****************************section-one**************************************************-->
<div><?php include 'header.php'; ?></div><br><br>
<div>

</div><br>
<!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p><b>The Colombo Friend-in-Need Society (CFINS)</b> is the oldest Charitable Organisation in Sri Lanka. Established originally in the year 1831 under the Patronage of then Governor Sir Edward Barnes, the Society has now completed over 175 years of service to the people of Sri Lanka. The CFINS has continued to this day the tradition of having the country’s Head of State as its ex-officio Patron. It’s activities are managed and directed by a Board of Management consisting entirely of volunteers among whom are several leading members of the Medical Profession, Engineers, Company Directors and Executives from the Corporate Sector.</p>
                <p>The Jaipur Foot Programme, administered by CFINS, is the main provider of prosthetic limbs and orthotic appliances to disabled persons in Sri Lanka. It runs a prosthetic/orthotic Workshop in central Colombo, where disabled persons come in to get custom-fitted artificial limbs, developed under the Jaipur foot technique, to give them a new lease of life.</p>
                <p>Today The CFINS’s Limb-fitting Centre has become a one- stop service facility for all disabled persons who require prosthetic, orthotic and orthopaedic appliances to overcome their disabilities. Amputees from areas outside Colombo are provided free board and lodging in the Transit Hostel run by the CFINS, to enable them to stay in the premises until their limbs are fabricated, fitted and aligned and they are trained to use them. It is a unique and complete package of services no other Institution in Sri Lanka can provide.</p>
                <p>Through this service, CFINS endeavours to improve the quality of life and advance the rights of physically handicapped and disabled persons by.</p>
            </div>
        </div>
    </div>

<div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header" align="center">Our Board</h2>
            </div>

        </div>

        <div class="row">
          <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
      </div>
            <div class="row">
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
        </div>
</div>
<!-- staff --><br>
<div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header" align="center">Our Staff</h2>
            </div>

        </div>

        <div class="row">
          <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
      </div>
            <div class="row">
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
            <div class="col-lg-6 col-sm-6">
                <img class="img-circle img-responsive" src="img/1.png">
                <h3>Lahiru Dhananjaya
                    <small>CEO </small>
                </h3>
                <p>Sayings of partcular person!</p>
            </div>
        </div>

    </div>
<!--c -->

<?php //footer
include 'footer.php';
?>  

</body>
</html>