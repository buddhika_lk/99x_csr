<!doctype HTML>
<html lang="en" ng-app="app">
	
	<head>
	  <meta charset="utf-8">
	  	<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	  	<title>The Colombo Friend-In-Need Society</title>
                <!--font face links -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
   	<link href="slider/js-image-slider.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
   	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
    <script type="text/javascript" src="js/angular.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    </head>

<body>
<div><?php include 'header.php'; ?></div>

<!-- *****************************carousel**************************************************-->
<div class="carousel slide" id="myCarousel">
	
	<ol class="carousel-indicators">
		<li data-target="site-carousel" data-slide-to="0" class="active"></li>
		<li data-target="site-carousel" data-slide-to="1"></li>
		<li data-target="site-carousel" data-slide-to="2"></li>
		<li data-target="site-carousel" data-slide-to="3"></li>
	</ol>
	
	<div class="carousel-inner">
		
		<div class="item active">
			<img src="slider/slider/1.jpg">
			<div class="container">
				<div class="carousel-caption">
					<h2>Sport Meet</h2>
					<p>Sport Meet for Children</p>
				</div>
			</div>	
		</div>
		<div class="item">
			<img src="slider/slider/2.jpg">
			<div class="container">
				<div class="carousel-caption">
					<h2>Sport Meet</h2>
					<p>Sport Meet for Children</p>
				</div>
			</div>	
		</div>
		<div class="item">
			<img src="slider/slider/3.jpg">
			<div class="container">
				<div class="carousel-caption">
					<h2>Sport Meet</h2>
					<p>Sport Meet for Children</p>
				</div>
			</div>	
		</div>
		<div class="item">
			<img src="slider/slider/12.jpg">
			<div class="container">
				<div class="carousel-caption">
					<h2>Sport Meet</h2>
					<p>Sport Meet for Children</p>
				</div>
			</div>	
		</div>
	
	</div>

    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
	
</div><div style="clear:both;"></div>


<!-- *****************************motto**************************************************-->
<div class="main-text hidden-xs">
                <div class="col-md-12 text-center">
                    <h1>
                        The Colombo Friend-In-Need Society</h1>
                    <h3>
                        "THEY SHALL NOT SUFFER"
                    </h3><hr/>
                    
    <hr/>
                </div>
 </div>

<!-- *****************************Donation**************************************************-->
<div class="container ">
		<div class="col-md-12">
			<div class="col-md-8">
				<div class="row">
					<div class="col-xs-18">
						<div>
							 <div class="well">
        <h1 class="text-center" style="color: green;">Donation Required</h1>
        <div class="list-group">
          <a href="#" class="list-group-item ">
                <div class="media col-md-3">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive"  src="http://placehold.it/350x250" alt="placehold.it/350x250" >
                    </figure>
                </div>
                <div class="col-md-6">
                    <h4 class="list-group-item-heading"> List group heading </h4>
                    <p class="list-group-item-text"> Qui diam libris ei, vidisse incorrupte at mel. His euismod salutandi dissentiunt eu. Habeo offendit ea mea. Nostro blandit sea ea, viris timeam molestiae an has. At nisl platonem eum. 
                        Vel et nonumy gubergren, ad has tota facilis probatus. Ea legere legimus tibique cum, sale tantas vim ea, eu vivendo expetendis vim. Voluptua vituperatoribus et mel, ius no elitr deserunt mediocrem. Mea facilisi torquatos ad.
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <h2> 14240 <small> votes </small></h2>
                    <button type="button" class="btn btn-default btn-lg btn-block"> Donate Now! </button>
                    <div class="stars">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p> Average 4.5 <small> / </small> 5 </p>
                </div>
          </a>
          <a href="#" class="list-group-item">
                <div class="media col-md-3">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250" >
                    </figure>
                </div>
                <div class="col-md-6">
                    <h4 class="list-group-item-heading"> List group heading </h4>
                    <p class="list-group-item-text"> Eu eum corpora torquatos, ne postea constituto mea, quo tale lorem facer no. Ut sed odio appetere partiendo, quo meliore salutandi ex. Vix an sanctus vivendo, sed vocibus accumsan petentium ea. 
                        Sed integre saperet at, no nec debet erant, quo dico incorrupte comprehensam ut. Et minimum consulatu ius, an dolores iracundia est, oportere vituperata interpretaris sea an. Sed id error quando indoctum, mel suas saperet at.                         
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <h2> 12424 <small> votes </small></h2>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Donate Now!</button>
                    <div class="stars">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p> Average 3.9 <small> / </small> 5 </p>
                </div>
          </a>
          <a href="#" class="list-group-item">
                <div class="media col-md-3">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250">
                    </figure>
                </div>
                <div class="col-md-6">
                    <h4 class="list-group-item-heading"> List group heading </h4>
                    <p class="list-group-item-text"> Ut mea viris eripuit theophrastus, cu ponderum accusata consequuntur cum. Suas quaestio cotidieque pro ea. Nam nihil persecuti philosophia id, nam quot populo ea. 
                        Falli urbanitas ei pri, eu est enim volumus, mei no volutpat periculis. Est errem iudicabit cu. At usu vocibus officiis, ad ius eros tibique appellantur.                         
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <h2> 13540 <small> votes </small></h2>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Donate Now!</button>
                    <div class="stars">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p> Average 4.1 <small> / </small> 5 </p>
                </div>
          </a>
          <a href="#" class="list-group-item">
                <div class="media col-md-3">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250">
                    </figure>
                </div>
                <div class="col-md-6">
                    <h4 class="list-group-item-heading"> List group heading </h4>
                    <p class="list-group-item-text"> Ut mea viris eripuit theophrastus, cu ponderum accusata consequuntur cum. Suas quaestio cotidieque pro ea. Nam nihil persecuti philosophia id, nam quot populo ea. 
                        Falli urbanitas ei pri, eu est enim volumus, mei no volutpat periculis. Est errem iudicabit cu. At usu vocibus officiis, ad ius eros tibique appellantur.                         
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <h2> 13540 <small> votes </small></h2>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Donate Now!</button>
                    <div class="stars">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p> Average 4.1 <small> / </small> 5 </p>
                </div>
          </a>
          <a href="#" class="list-group-item">
                <div class="media col-md-3">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="http://placehold.it/350x250" alt="placehold.it/350x250">
                    </figure>
                </div>
                <div class="col-md-6">
                    <h4 class="list-group-item-heading"> List group heading </h4>
                    <p class="list-group-item-text"> Ut mea viris eripuit theophrastus, cu ponderum accusata consequuntur cum. Suas quaestio cotidieque pro ea. Nam nihil persecuti philosophia id, nam quot populo ea. 
                        Falli urbanitas ei pri, eu est enim volumus, mei no volutpat periculis. Est errem iudicabit cu. At usu vocibus officiis, ad ius eros tibique appellantur.                         
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <h2> 13540 <small> votes </small></h2>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Donate Now!</button>
                    <div class="stars">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
                    </div>
                    <p> Average 4.1 <small> / </small> 5 </p>
                </div>
          </a>
        </div>
        </div>

<style type="text/css">
    a.list-group-item {
    height:auto;
    min-height:220px;
}
a.list-group-item.active small {
    color:#fff;
}
.stars {
    margin:20px auto 1px;    
}
</style>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 visible-lg">
				<div class=".media .pull-left">
					<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FColomboFriendInNeedSociety%3Ffref%3Dts&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=645080792271695" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true"></iframe>
				</div><br><br><br>
				<hr/>
				<div>
					<div class=".media .pull-left">
					<h4>Upcoming Events</h4>
						
						<div class="media">
							<div class="media-body" id="cal-heading">
								<div class="pull-left" id="glyp-icon"><i class="fa fa-calendar fa-3x"></i></div>
								<span class="media-heading"><b>Youth medical camp</b><br></span>
								<small class="muted">On 17 Aug 2013</small>
							</div>
						</div>
						<div class="media">
							<div class="media-body" id="cal-heading">
								<div class="pull-left" id="glyp-icon"><i class="fa fa-calendar fa-3x"></i></div>
								<span class="media-heading"><b>Youth medical camp</b><br></span>
								<small class="muted">On 17 Aug 2013</small>
							</div>
						</div>
						<div class="media">
							<div class="media-body" id="cal-heading">
								<div class="pull-left" id="glyp-icon"><i class="fa fa-calendar fa-3x"></i></div>
								<span class="media-heading"><b>Youth medical camp</b><br></span>
								<small class="muted">On 17 Aug 2013</small>
							</div>
						</div>
					</div>
				</div>
				<!-- success story link-->
				<hr/>
				<div class=".media .pull-left">
					<h4 style="color: red; padding-left:100px;">Success Stories</h4>
					<div class="list-group">
						  <a href="#" class="list-group-item ">
						    <h4 class="list-group-item-heading">Youth Engage - Workshop on youth participation</h4>
						    <p class="list-group-item-text"><img src="img/P001.jpg" class="img-responsive" alt="Responsive image">Youth Participation and Skills Development' - Conducted by Prof. Christopher Dekki, Coordinator of the United Nations Youth Advocacy Team, Member of the IYTF of the World Conference on Youth 2014....</p>
						  </a>
						  <a href="#" class="list-group-item ">
						    <h4 class="list-group-item-heading">Youth Engage - Workshop on youth participation</h4>
						    <p class="list-group-item-text"><img src="img/P001.jpg" class="img-responsive" alt="Responsive image">Youth Participation and Skills Development' - Conducted by Prof. Christopher Dekki, Coordinator of the United Nations Youth Advocacy Team, Member of the IYTF of the World Conference on Youth 2014....</p>
						  </a>
					</div>
				</div>
				<div>

				</div>

			</div>
		</div>
	</div>
    
Angular Testing
<div ng-controller="MainController">
    <div  ng-repeat="user in mydata.arr">
            {{user}}aa
        </div>
</div>

<!-- *****************************end of Donation**************************************************-->
	<div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<?php include 'footer.php' ?>
<!-- JavaScript -->
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script>
		$(document).ready(function(){
			$('#myCarousel').carousel({
				interval: 5000
			});
		});

		$(document).ready(function(){
			$('#testimonials').carousel({
				interval: 5000
			});
		});
	</script>
</body>
</html>