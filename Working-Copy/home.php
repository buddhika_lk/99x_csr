<!doctype HTML>
<html lang="en" ng-app="app">
  
  <head>
    <meta charset="utf-8">
      <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

      <title>The Colombo Friend-In-Need Society</title>
                <!--font face links -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="slider/js-image-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/style_timeline.css" />
    <link href='css/Montserrat.css' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
    <script type="text/javascript" src="js/angular.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    </head>

<body>
<div><?php
include 'header.php'; ?></div>

<!-- *****************************carousel**************************************************-->
<div class="carousel slide" id="myCarousel" style="border-radius: 50px;">
  
  <ol class="carousel-indicators">
    <li data-target="site-carousel" data-slide-to="0" class="active"></li>
    <li data-target="site-carousel" data-slide-to="1"></li>
    <li data-target="site-carousel" data-slide-to="2"></li>
    <li data-target="site-carousel" data-slide-to="3"></li>
  </ol>
  
  <div class="carousel-inner" id="mossbol">
    
    <div class="item active">
      <img src="slider/slider/s01.jpg">
      <div class="container">
        <div class="carousel-caption">
          <h2 style="font-family:moss;">The Colombo Friend-In-Need Society</h2>
          <p style="font-family:moss;">THEY SHALL NOT SUFFER</p>
        </div>
      </div>  
    </div>
    <div class="item">
      <img src="slider/slider/s02.jpg">
      <div class="container">
        <div class="carousel-caption">
          <h2 style="font-family:moss;">Workshops</h2>
          <p style="font-family:moss;">For North Province People</p>
        </div>
      </div>  
    </div>
    <div class="item">
      <img src="slider/slider/s03.jpg">
      <div class="container">
        <div class="carousel-caption">
          <h2 style="font-family:moss;">Donations</h2>
          <p style="font-family:moss;">By University Students</p>
        </div>
      </div>  
    </div>
    <div class="item">
      <img src="slider/slider/s04.jpg">
      <div class="container">
        <div class="carousel-caption">
          <h2 style="font-family:moss;">Sport Meet</h2>
          <p style="font-family:moss;">Sport Meet for Disable's</p>
        </div>
      </div>  
    </div>
  
  </div>

    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev" style="margin-left:25px;">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next" style="margin-right:25px;">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  
</div><div style="clear:both;"></div>


<!-- *****************************motto**************************************************-->
<div class="container">
<div class="responsive">
                <div class="col-md-12 text-center">
                    <h1 style="font-family:moss;">
                        The Colombo Friend-In-Need Society</h1>
                    <h3 style="font-family:moss;">
                        "THEY SHALL NOT SUFFER"
                    </h3>
                    <hr/>
                </div>
                    <div><br>
                      <div class="col-xs-6 col-sm-4" style="padding-top:20px;">
                        <center>
                          <a target="_blank" href="https://docs.google.com/forms/d/1V5yzu5tB3cScYAu-2SzC_bpWledfefwmt1JWuUy7rFI/viewform">
                        <img class="img-responsive" src="images/be_member.png">
                          </a>
                      </center>
                      </div>
                      
                      <div class="col-xs-6 col-sm-4">
                        <center>
                          <a target="_blank" href="https://docs.google.com/forms/d/1V5yzu5tB3cScYAu-2SzC_bpWledfefwmt1JWuUy7rFI/viewform">
                        <img class="img-responsive" src="images/donate.png">
                          </a>
                      </center>
                      </div>

                      <div class="col-xs-6 col-sm-4">
                        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FColomboFriendInNeedSociety%3Ffref%3Dts&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=645080792271695" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true"></iframe>
                      </div>

                    </div>
    <br><br>
                
 </div>
</div><br><br><hr>
<!-- *****************************Donation**************************************************-->
<div class="container">
  <center>
    <span class="info">We Achieved ...</span>
  </center>
<div class="timelinecontainer" >
  <div class="img-responsive">
     <div class="row tile">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon1.png" width="200px" class="img-responsive"/>
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year">From 1831</span> <br />
            <span class="info">Serving 12500+ <br />Patients</span>
         </div>
      </div>
      <div class="row tile">
       <div class="col-md-6 col-sm-6 textarea-right">
            <span class="year">Conducting</span> <br />
            <span class="info">over 1000 Mobile<br />Workshops</span>
         </div>
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon2.png" width="200px" class="img-responsive" />
        </div>
      </div>
      <div class="row tile">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon6.png" width="200px" class="img-responsive" />
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year">Since last 20 years</span> <br />
            <span class="info">Aidex</span>
         </div>
      </div>
      <div class="row tile">
       <div class="col-md-6 col-sm-6 textarea-right">
            <span class="year"></span> <br />
            <span class="info">2000+ donors</span>
         </div>
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon4.png" width="200px" class="img-responsive"/>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon5.png" width="200px" class="img-responsive" />
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year"></span> <br />
            <span class="info">Serving FREE</span>
         </div>
      </div>
    </div>
  </div>
<div>

    
<!-- *****************************end of Donation**************************************************-->
  <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<style type="text/css">
    a.list-group-item {
    height:auto;
    min-height:220px;
}
a.list-group-item.active small {
    color:#fff;
}
.stars {
    margin:20px auto 1px;    
}
</style>
<!-- JavaScript -->
  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script>
    $(document).ready(function(){
      $('#myCarousel').carousel({
        interval: 5000
      });
    });

    $(document).ready(function(){
      $('#testimonials').carousel({
        interval: 5000
      });
    });
  </script>
</div>
<?php
include 'footer.php' ?>
</body>
</html>