<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Colombo Friend-In-Need Society</title>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css"> 
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='css/Montserrat.css' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    
</head>

<body>
<!-- <img class="brand" src="img/Logo.jpg" width="50px" />-->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
    <a class="span1" href="index.php" ></a>
      </div>
      <div id="navbar" class="navbar-collapse collapse" >
        <div class="mainL"><div class="mainlogo"><img class="img-responsive" src="img/menu_logo.jpg" alt=""></div></div>
        <ul class="nav navbar-nav pull-right">
              <li><a href="home.php">Home</a></li>
              <li><a href="about.php">About Us</a></li>
              <li><a href="services.php">Services</a></li>
                    <li><a href="gallery.php">Gallery</a></li>
              <li><a href="contact.php">Contact Us</a></li>
      </ul>           
    </div>

<!--
    <div id="navbar" class="navbar-collapse collapse">
      <div class="mainL"><div class="mainlogo"><img class="img-responsive" src="img/menu_logo.jpg" alt=""></div></div>
            <ul class="nav navbar-nav">
              <li><a href="home.php">Home</a></li>
              <li><a href="about.php">About Us</a></li>
              <li><a href="services.php">Services</a></li>
                    <li><a href="gallery.php">Gallery</a></li>
              <li><a href="contact.php">Contact Us</a></li>
                </ul>
              
            
          </div>
        -->

  </div>
</div><!--end cssmenu--> 
 
<style type="text/css">
  .navbar-default {
  background-color: #a50c10;
  border-color: #da4551;
  height: 50px;
}
.navbar-default .navbar-brand {
  color: #ffffff;
}
.navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
  color: #d8d3dc;
}
.navbar-default .navbar-text {
  color: #ffffff;
}
.navbar-default .navbar-nav > li > a {
  color: #ffffff;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
  color: #d8d3dc;
}
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
  color: #d8d3dc;
  background-color: #da4551;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  color: #d8d3dc;
  background-color: #da4551;
}
.navbar-default .navbar-toggle {
  border-color: #da4551;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
  background-color: #da4551;
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #ffffff;
}
.navbar-default .navbar-collapse,
.navbar-default .navbar-form {
  border-color: #ffffff;
}
.navbar-default .navbar-link {
  color: #ffffff;
}
.navbar-default .navbar-link:hover {
  color: #d8d3dc;
}

@media (max-width: 767px) {
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #ffffff;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: #d8d3dc;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > .active > a, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
    color: #d8d3dc;
    background-color: #da4551;
  }
}
</style>