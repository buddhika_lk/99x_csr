<html>
<head>
  <title></title>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
</head>
<body>
<div>
    <?php
      include 'header.php';
     ?>
     <div>
<div class="container">
<br><br>
<div class="resume">
    <header class="page-header">
    <h1 class="page-title">Self Employment</h1><br>
    <p><h5 align="left">Through our self-employment project, our aim is to help amputees and disabled persons to regain their self-esteem, dignity and sense of well being and re-integrate into mainstream society.</h5></p><br>
    <p><h5 align="left">CFINS also provides needy amputees and disabled persons with interest free loans to enable them to undertake income generating activities so that they could support themselves and their families, without being a burden to others.</h5></p>
  </header>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P011.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br>
                <li class="list-group-item"><b>Name     :</b>  T.S. Danushka Perera</li>
                <li class="list-group-item"><b>Age :</b>  21 yrs</li>
                <li class="list-group-item"><b>Cause of disability :</b>  Train accident</li>
                <li class="list-group-item"><b>Family background :</b>  Father – electrician, Mother – housewife, Sister – schooling, Brother – unemployed
Hopes to help mother start a sewing business</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P009.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br>
                <li class="list-group-item"><b>Name     :</b>  Nishantha Nananyakkara</li>
                <li class="list-group-item"><b>Age :</b>  26 yrs</li>
                <li class="list-group-item"><b>Cause of disability :</b>  Cancer</li>
                <li class="list-group-item"><b>Family background :</b>  Father of 2 children (1.5 and 3 yrs), wife is a teacher
Hopes to start a phone accessories shop</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P012.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br>
                <li class="list-group-item"><b>Name     :</b>  M. D. Amarasiri</li>
                <li class="list-group-item"><b>Age :</b>  61 yrs</li>
                <li class="list-group-item"><b>Cause of disability :</b>  Accident</li>
                <li class="list-group-item"><b>Family background :</b>Wants to buy a three wheeler</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P010.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br>
                <li class="list-group-item"><b>Name     :</b>  Ranjith Kumara</li>
                <li class="list-group-item"><b>Age :</b>  29 yrs</li>
                <li class="list-group-item"><b>Cause of disability :</b>  Accident</li>
                <li class="list-group-item"><b>Family background :</b>Hopes to start a grocery shop or drive a three-wheeler.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

</div>
</div>
</div>
<div>
  <?php
    include 'footer.php';
   ?>
</div>
</div>
<style type="text/css">

.page-header{
  text-align: center;    
}

/*social buttons*/
.btn-social{
  color: white;
  opacity:0.9;
}
.btn-social:hover {
  color: white;
    opacity:1;
}
.btn-facebook {
background-color: #3b5998;
opacity:0.9;
}
.btn-twitter {
background-color: #00aced;
opacity:0.9;
}
.btn-linkedin {
background-color:#0e76a8;
opacity:0.9;
}
.btn-github{
  background-color:#000000;
  opacity:0.9;
}
.btn-google {
  background-color: #c32f10;
  opacity: 0.9;
}
.btn-stackoverflow{
  background-color: #D38B28;
  opacity: 0.9;
}

/* resume stuff */

.bs-callout {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #eee;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 1px 5px;
    margin-bottom: 5px;
    padding: 20px;
}
.bs-callout:last-child {
    margin-bottom: 0px;
}
.bs-callout h4 {
    margin-bottom: 10px;
    margin-top: 0;
}

.bs-callout-danger {
    border-left-color: #d9534f;
}

.bs-callout-danger h4{
    color: #d9534f;
}

.resume .list-group-item:first-child, .resume .list-group-item:last-child{
  border-radius:0;
}

/*makes an anchor inactive(not clickable)*/
.inactive-link {
   pointer-events: none;
   cursor: default;
}

.resume-heading .social-btns{
  margin-top:15px;
}
.resume-heading .social-btns i.fa{
  margin-left:-5px;
}



@media (max-width: 992px) {
  .resume-heading .social-btn-holder{
    padding:5px;
  }
}


/* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */

.progress-bar {
    text-align: left;
  white-space: nowrap;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  cursor: pointer;
}

.progress-bar > .progress-type {
  padding-left: 10px;
}

.progress-meter {
  min-height: 15px;
  border-bottom: 2px solid rgb(160, 160, 160);
  margin-bottom: 15px;
}

.progress-meter > .meter {
  position: relative;
  float: left;
  min-height: 15px;
  border-width: 0px;
  border-style: solid;
  border-color: rgb(160, 160, 160);
}

.progress-meter > .meter-left {
  border-left-width: 2px;
}

.progress-meter > .meter-right {
  float: right;
  border-right-width: 2px;
}

.progress-meter > .meter-right:last-child {
  border-left-width: 2px;
}

.progress-meter > .meter > .meter-text {
  position: absolute;
  display: inline-block;
  bottom: -20px;
  width: 100%;
  font-weight: 700;
  font-size: 0.85em;
  color: rgb(160, 160, 160);
  text-align: left;
}

.progress-meter > .meter.meter-right > .meter-text {
  text-align: right;
}


</style>
</body>
</html>