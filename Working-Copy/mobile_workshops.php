<html>
<head>
  <title></title>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href='css/Montserrat.css' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
</head>
<body>
<div>
    <?php
      include 'header.php';
     ?>
</div><br><br>
<div class="page-header" align="center">
    <div>
      <h1 class="page-title">Mobile Workshops</h1><br>
    <p><h5 align="left">In September 2001, the Mobile Workshop was commissioned in order to bring our services within easy reach of the disabled persons living in remote areas of the country. Since then it has been deployed regularly for duty in outlying areas to deliver most of the services available at the Workshop and nearly 20,000 have been served up to September 2009.</h5></p><br>
    <p><h5 align="left"></h5><b>Mobile Workshop and Service Unit 
(carried out after the liberation of the North East).</b>With the war coming to an end last year, CFINS geared itself to rehabilitate the vast number of disabled and war wounded Sri Lankans caught in the crossfire. Since then, the Mobile Workshop was deployed almost once a month in the Vanni and the Eastern regions benefiting several hundred amputees.</p>
  <br>
    </div>

</div>
  
<div class="col-md-4"></div>
<!--
  <div>
    <center>
  <div class="col-md-4">
    <table class="table">
          <tr>
            <td ><p> Month </p></td>
            <td ><p> Location </p></td>
            <td ><p> No. of Limbs Made </p></td>
          </tr>
          <tr>
            <td ><p> April </p></td>
            <td ><p> Trincomalee </p></td>
            <td ><p> 40 </p></td>
          </tr>
          <tr>
            <td ><p> July </p></td>
            <td ><p> Mannar </p></td>
            <td ><p> 139 </p></td>
          </tr>
          <tr>
            <td ><p> August </p></td>
            <td ><p> Vavuniya </p></td>
            <td ><p> 116 </p></td>
          </tr>
          <tr>
            <td ><p> September </p></td>
            <td ><p> Mannar </p></td>
            <td ><p> 39 </p></td>
          </tr>
          <tr>
            <td ><p> October </p></td>
            <td ><p> Chettikulam </p></td>
            <td ><p> 105 </p></td>
          </tr>
          <tr>
            <td ><p> November </p></td>
            <td ><p> Pompamadu </p></td>
            <td ><p> 83 </p></td>
          </tr>
          <tr>
            <td ><p> Feb/March </p></td>
            <td ><p> Chettikulam </p></td>
            <td ><p> 74 </p></td>
          </tr>
        </table>
  </div>
  </center><br>
  </div> -->
<div class="container">
    <ul class="timeline">
        <li >
            <div class="timeline-badge"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">24 -26 th April 2009  at Trincomalee (Jayasumanaramaya Temple)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 65 </p></td>
            <td ><p > 40 </p></td>
            <td ><p > 3 </p></td>
            <td ><p > 0 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Trinco001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Trinco002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>
        <!-- Next --><br><br>
        <li class="timeline-inverted">
            <div class="timeline-badge info"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">8 th - 23 rd July 2009 - Mannar (District Hospital)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 146 </p></td>
            <td ><p > 139 </p></td>
            <td ><p > 8 </p></td>
            <td ><p > 0 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td ><img src="images/Mannar001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td ><div align="right"><img src="images/Mannar002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>
        <!-- Next -->
        <li>
            <div class="timeline-badge"></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">14 th - 28 th August 2009 - Vavuniya (District Hospital)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 169 </p></td>
            <td ><p > 116 </p></td>
            <td ><p > 0 </p></td>
            <td ><p > 19 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Vavuniya001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Vavuniya002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>

        <!-- Next -->
        <li class="timeline-inverted">
            <div class="timeline-badge info"></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">18 th - 28 th September 2009 - Mannar (District Hospital & IDP Camps)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 169 </p></td>
            <td ><p > 116 </p></td>
            <td ><p > 0 </p></td>
            <td ><p > 19 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Mannar2001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Mannar2002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>

        <!-- Next -->
        <li>
            <div class="timeline-badge"></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">26 th October - 9 th November 2009 - Chettikulam (IDP Camp)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 122 </p></td>
            <td ><p > 105 </p></td>
            <td ><p > 0 </p></td>
            <td ><p > 18 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Chettikulam001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Chettikulam002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>

        <!-- Next -->
        <li class="timeline-inverted">
            <div class="timeline-badge info"></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">27 th November - 7 th December 2009 - Ponpaimadu (Rehabilitation Centre)</h4><br>
              
            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 129 </p></td>
            <td ><p > 83 </p></td>
            <td ><p > 0 </p></td>
            <td ><p > 34 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Vavuniya2002.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Vavuniya2001.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>

        <!-- Next -->
        <li>
            <div class="timeline-badge"></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title" style="font-weight:bold;">19 th February - 2 nd March 2010 - Chettikulam (Anandakumaraswamy Camp)</h4><br>

            </div>
            <div class="timeline-body">
      <p><table class="table table-bordered">
          <tbody><tr>
            <td><p > No. of Attendees </p></td>
            <td ><p> New Limbs Fitted </p></td>
            <td ><p > Measurements / Casts Taken </p></td>
            <td ><p > Repairs to Prostheses </p></td>
          </tr>
          <tr>
            <td ><p > 154 </p></td>
            <td ><p > 74 </p></td>
            <td ><p > 0 </p></td>
            <td ><p > 27 </p></td>
          </tr>
        </tbody></table>
<table width="310" border="0" align="center">
          <tbody><tr>
            <td width="150"><img src="images/Chettikulam2001.jpg" width="150" height="100" alt="" class="img-thumbnail"></td>
            <td width="150"><div align="right"><img src="images/Chettikulam2002.jpg" width="150" height="100" alt="" class="img-thumbnail"><br>
            </div></td>
          </tr>
        </tbody></table>
    </p>

            </div>
          </div>
        </li>
        
    </ul>
</div>
<div>
  <?php
    include 'footer.php';
   ?>
</div>
</div>
<style type="text/css">
  .timeline {
  list-style: none;
  padding: 20px 0 20px;
  position: relative;
}
.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #FFFFFF;
  right: 25px;
  margin-left: -1.5px;
}
.timeline > li {
  margin-bottom: 20px;
  position: relative;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li > .timeline-panel {
  width: calc( 100% - 75px );
  float: left;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}
.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}
.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 1.4em;
  text-align: center;
  position: absolute;
  top: 16px;
  right: 0px;
  margin-left: -25px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}
.timeline > li.timeline-inverted > .timeline-panel {
  float: right;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
.timeline-badge.primary {
  background-color: #2e6da4 !important;
}
.timeline-badge.success {
  background-color: #3f903f !important;
}
.timeline-badge.warning {
  background-color: #f0ad4e !important;
}
.timeline-badge.danger {
  background-color: #d9534f !important;
}
.timeline-badge.info {
  background-color: #5bc0de !important;
}
.timeline-title {
  margin-top: 0;
  color: inherit;
}
.timeline-body > p,
.timeline-body > ul {
  margin-bottom: 0;
}
.timeline-body > p + p {
  margin-top: 5px;
}
</style>
</body>
</html>