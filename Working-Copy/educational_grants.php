<html>
<head>
  <title></title>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <script type="text/javascript" src="slider/js-image-slider.js" ></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
</head>
<body>
<div>
    <?php
      include 'header.php';
     ?>
     <div>
      <div class="year">
<div class="container">
<br><br>
<div class="resume">
    <header class="page-header">
    <h1 class="page-title">Educational Grants</h1><br>
    <p><h5>Our aim is to help disabled children continue their schooling without interruption and to develop their full potential by providing educational grants</h5></p>
  </header>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
    <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P006.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br><br>
                <li class="list-group-item"><b>Name     :</b>  Eranga Pradeep Weerakoon</li>
                <li class="list-group-item"><b>District :</b>  Galle</li>
                <li class="list-group-item"><b>Siblings :</b>  2 brothers</li>
              </ul>
            </div>
          </div>
        </div>
      </div>


  </div>
  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P007.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br><br>
                <li class="list-group-item"><b>Name     :</b>  K.Thushanthini</li>
                <li class="list-group-item"><b>District :</b>  Nuwara Eliya</li>
                <li class="list-group-item"><b>Siblings :</b>  3 sisters</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P008.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br><br>
                <li class="list-group-item"><b>Name     :</b>  Mohamed Risard</li>
                <li class="list-group-item"><b>District :</b>  Anuradhapura</li>
                <li class="list-group-item"><b>Siblings :</b>  2 sisters</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

  <!-- next-->
  <div class="panel panel-default">
      <div class="panel-heading resume-heading">
        <div class="row">
          <div class="col-lg-12">
            <div class="col-xs-12 col-sm-4">
              <figure>
                <img class="img-circle img-responsive" alt="" src="images/P017.jpg"/>
              
              
            </div>

            <div class="col-xs-12 col-sm-8">
              <ul class="list-group"><br><br>
                <li class="list-group-item"><b>Name     :</b>  K.T. Dilshan Perera</li>
                <li class="list-group-item"><b>District :</b>  Gampaha</li>
                <li class="list-group-item"><b>Siblings :</b>  1 sisters</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  </div>

</div>
</div>
</div>
<div>
  <?php
    include 'footer.php';
   ?>
</div>
</div>
</div>
<style type="text/css">

.page-header{
  text-align: center;    
}

/*social buttons*/
.btn-social{
  color: white;
  opacity:0.9;
}
.btn-social:hover {
  color: white;
    opacity:1;
}
.btn-facebook {
background-color: #3b5998;
opacity:0.9;
}
.btn-twitter {
background-color: #00aced;
opacity:0.9;
}
.btn-linkedin {
background-color:#0e76a8;
opacity:0.9;
}
.btn-github{
  background-color:#000000;
  opacity:0.9;
}
.btn-google {
  background-color: #c32f10;
  opacity: 0.9;
}
.btn-stackoverflow{
  background-color: #D38B28;
  opacity: 0.9;
}

/* resume stuff */

.bs-callout {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #eee;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px 1px 1px 5px;
    margin-bottom: 5px;
    padding: 20px;
}
.bs-callout:last-child {
    margin-bottom: 0px;
}
.bs-callout h4 {
    margin-bottom: 10px;
    margin-top: 0;
}

.bs-callout-danger {
    border-left-color: #d9534f;
}

.bs-callout-danger h4{
    color: #d9534f;
}

.resume .list-group-item:first-child, .resume .list-group-item:last-child{
  border-radius:0;
}

/*makes an anchor inactive(not clickable)*/
.inactive-link {
   pointer-events: none;
   cursor: default;
}

.resume-heading .social-btns{
  margin-top:15px;
}
.resume-heading .social-btns i.fa{
  margin-left:-5px;
}



@media (max-width: 992px) {
  .resume-heading .social-btn-holder{
    padding:5px;
  }
}


/* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */

.progress-bar {
    text-align: left;
  white-space: nowrap;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  cursor: pointer;
}

.progress-bar > .progress-type {
  padding-left: 10px;
}

.progress-meter {
  min-height: 15px;
  border-bottom: 2px solid rgb(160, 160, 160);
  margin-bottom: 15px;
}

.progress-meter > .meter {
  position: relative;
  float: left;
  min-height: 15px;
  border-width: 0px;
  border-style: solid;
  border-color: rgb(160, 160, 160);
}

.progress-meter > .meter-left {
  border-left-width: 2px;
}

.progress-meter > .meter-right {
  float: right;
  border-right-width: 2px;
}

.progress-meter > .meter-right:last-child {
  border-left-width: 2px;
}

.progress-meter > .meter > .meter-text {
  position: absolute;
  display: inline-block;
  bottom: -20px;
  width: 100%;
  font-weight: 700;
  font-size: 0.85em;
  color: rgb(160, 160, 160);
  text-align: left;
}

.progress-meter > .meter.meter-right > .meter-text {
  text-align: right;
}


</style>
</body>
</html>