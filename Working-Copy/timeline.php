<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Untitled Document</title>
        <link rel="stylesheet" type="text/css" href="css/style_timeline.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        
<!--        Google Fonts-->
       <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        
    </head>
  <body>
  
  <div class="timelinecontainer">
     <div class="row tile">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon1.png" width="200px" />
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year">From 1831</span> <br />
            <span class="info">Serving 12500+ <br />Patients</span>
         </div>
      </div>
      <div class="row tile">
       <div class="col-md-6 col-sm-6 textarea-right">
            <span class="year">Conducting</span> <br />
            <span class="info">over 1000 Mobile<br />Workshops</span>
         </div>
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon2.png" width="200px" />
        </div>
      </div>
      <div class="row tile">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon3.png" width="200px" />
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year">Since last 20 years</span> <br />
            <span class="info">Aidex</span>
         </div>
      </div>
      <div class="row tile">
       <div class="col-md-6 col-sm-6 textarea-right">
            <span class="year"></span> <br />
            <span class="info">2000+ donors</span>
         </div>
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon4.png" width="200px" />
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6 icons">
            <img src="img/icon5.png" width="200px" />
        </div>
        <div class="col-md-6 col-sm-6 textarea">
            <span class="year"></span> <br />
            <span class="info">Serving FREE</span>
         </div>
      </div>
  </div>
  
  <script src="js/bootstrap.min.js"></script>
</body>
</html>